# Copyright 2018 Hugo Rodrigues
# License BSD-3-Clause
# pylint: disable=missing-docstring
{
    "name": "Website Analytics - Website Integragion",
    "summary": "Integrate Website Analytics with Odoo Website",
    "version": "12.0.1.0.1",
    "category": "Website Analytics",
    "website": "https://hugorodrigues.net",
    "author": "Hugo Rodrigues",
    "license": "Other OSI approved licence",
    "installable": True,
    "depends": [
        "website",
        "website_analytics",
        ],
    "data": [
        "security/ir.model.access.csv",
        "views/res_config_settings_views.xml",
        "views/templates.xml"
        ],
    "demo": ["demo/demo.xml"]
}
