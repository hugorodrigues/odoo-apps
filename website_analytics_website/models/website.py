"""Integration with website.analytics"""


from odoo import models, fields


class Website(models.Model):
    """
    Extend Odoo website module
    """
    _inherit = "website"

    website_analytics_id = fields.Many2one(comodel_name="website.analytics",
                                           string="Analytics")
