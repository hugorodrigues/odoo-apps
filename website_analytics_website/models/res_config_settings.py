"""Website analytics configuration for website"""

from odoo import fields, models


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    website_analytics_id = fields.Many2one(related="website_id.website_analytics_id",
                                           string="Analytics")
