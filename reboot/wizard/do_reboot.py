"""Reboots Odoo"""
# -*- coding: utf-8 -*-

from openerp import models, api, fields


class RebootLog(models.TransientModel):
    """Logs when Odoo was rebooted"""
    _name = 'do.reboot'

    motive = fields.Char(required=True)

    @api.multi
    def do_reboot(self):
        """Do a reboot"""
        self.env['reboot.reboot'].reboot(self.motive)
