# Translation of Odoo Server.
# This file contains the translation of the following modules:
#	* reboot
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 9.0e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-01-13 23:41+0000\n"
"PO-Revision-Date: 2018-01-13 23:41+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: reboot
#: model:ir.ui.view,arch_db:reboot.do_reboot
msgid "Cancel"
msgstr "Cancelar"

#. module: reboot
#: selection:reboot.log,state:0
msgid "Complete"
msgstr "Concluído"

#. module: reboot
#: model:ir.model.fields,field_description:reboot.field_do_reboot_create_uid
#: model:ir.model.fields,field_description:reboot.field_reboot_log_create_uid
msgid "Created by"
msgstr "Criada por"

#. module: reboot
#: model:ir.model.fields,field_description:reboot.field_do_reboot_create_date
#: model:ir.model.fields,field_description:reboot.field_reboot_log_create_date
msgid "Created on"
msgstr "Criado em"

#. module: reboot
#: model:ir.model.fields,field_description:reboot.field_do_reboot_display_name
#: model:ir.model.fields,field_description:reboot.field_reboot_log_display_name
#: model:ir.model.fields,field_description:reboot.field_reboot_reboot_display_name
msgid "Display Name"
msgstr "Nome a Mostrar"

#. module: reboot
#: model:ir.model.fields,field_description:reboot.field_reboot_log_fail_message
msgid "Fail message"
msgstr "Mensagem de erro"

#. module: reboot
#: selection:reboot.log,state:0
msgid "Failed"
msgstr "Falhou"

#. module: reboot
#: model:ir.model.fields,field_description:reboot.field_do_reboot_id
#: model:ir.model.fields,field_description:reboot.field_reboot_log_id
#: model:ir.model.fields,field_description:reboot.field_reboot_reboot_id
msgid "ID"
msgstr "Id."

#. module: reboot
#: model:ir.model.fields,field_description:reboot.field_do_reboot___last_update
#: model:ir.model.fields,field_description:reboot.field_reboot_log___last_update
#: model:ir.model.fields,field_description:reboot.field_reboot_reboot___last_update
msgid "Last Modified on"
msgstr "Última Modificação em"

#. module: reboot
#: model:ir.model.fields,field_description:reboot.field_do_reboot_write_uid
#: model:ir.model.fields,field_description:reboot.field_reboot_log_write_uid
msgid "Last Updated by"
msgstr "Última Atualização por"

#. module: reboot
#: model:ir.model.fields,field_description:reboot.field_do_reboot_write_date
#: model:ir.model.fields,field_description:reboot.field_reboot_log_write_date
msgid "Last Updated on"
msgstr "Última Atualização em"

#. module: reboot
#: model:ir.ui.menu,name:reboot.menu_reboot_log
msgid "Log"
msgstr "Histórico"

#. module: reboot
#: model:ir.model.fields,field_description:reboot.field_do_reboot_motive
#: model:ir.model.fields,field_description:reboot.field_reboot_log_motive
msgid "Motive"
msgstr "Motivo"

#. module: reboot
#: code:addons/reboot/models/reboot.py:74
#: sql_constraint:reboot.log:0
#, python-format
msgid "Motive must have at least 10 characters"
msgstr "Motivo tem de ter pelo menos 10 caracteres"

#. module: reboot
#: selection:reboot.log,state:0
msgid "Pending"
msgstr "Pendente"

#. module: reboot
#: model:ir.actions.act_window,name:reboot.action_do_reboot
#: model:ir.ui.menu,name:reboot.menu_do_reboot
#: model:ir.ui.menu,name:reboot.menu_reboot
#: model:ir.ui.view,arch_db:reboot.do_reboot
msgid "Reboot"
msgstr "Reiniciar"

#. module: reboot
#: model:ir.actions.act_window,name:reboot.action_reboot_log
msgid "Reboot Log"
msgstr "Histórico"

#. module: reboot
#: model:res.groups,name:reboot.reboot_log
msgid "Reboot Logs"
msgstr "Histórico"

#. module: reboot
#: model:res.groups,name:reboot.reboot_admin
msgid "Reboot System"
msgstr "Reiniciar Sistema"

#. module: reboot
#: model:ir.model.fields,field_description:reboot.field_reboot_log_state
msgid "State"
msgstr "Estado"

#. module: reboot
#: model:ir.ui.view,arch_db:reboot.reboot_log_form
#: model:ir.ui.view,arch_db:reboot.reboot_log_tree
msgid "User"
msgstr "Utilizador"

#. module: reboot
#: model:ir.ui.view,arch_db:reboot.reboot_log_form
#: model:ir.ui.view,arch_db:reboot.reboot_log_tree
msgid "When"
msgstr "Quando"

#. module: reboot
#: code:addons/reboot/models/reboot.py:41
#, python-format
msgid "You aren't allowed to reboot"
msgstr "Não está autorizado a reiniciar"

#. module: reboot
#: code:addons/reboot/models/reboot.py:103
#, python-format
msgid "You can't delete a log"
msgstr "Não é possível apagar o histórico"

#. module: reboot
#: code:addons/reboot/models/reboot.py:98
#, python-format
msgid "You can't duplicate a log"
msgstr "Não é possível duplicar entradas do histórico"

#. module: reboot
#: code:addons/reboot/models/reboot.py:93
#, python-format
msgid "You can't edit a reboot log"
msgstr "Não é possível editar o histórico"

#. module: reboot
#: model:ir.model,name:reboot.model_do_reboot
msgid "do.reboot"
msgstr "do.reboot"

#. module: reboot
#: model:ir.model,name:reboot.model_reboot_log
msgid "reboot.log"
msgstr "reboot.log"

#. module: reboot
#: model:ir.model,name:reboot.model_reboot_reboot
msgid "reboot.reboot"
msgstr "reboot.reboot"

