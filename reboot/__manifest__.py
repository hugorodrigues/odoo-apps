# -*- coding: utf-8 -*-
{
    'name': 'Reboot',
    'version': '1.0.1',
    'summary': 'Reboot Odoo',
    'author': 'Hugo Rodrigues',
    'website': 'https://hugorodrigues.net',
    'license': 'Other OSI approved licence',
    'category': 'Reboot',
    'depends': [
        'base'
    ],
    'data': [
        'security/groups.xml',
        'security/ir.model.access.csv',
        'views/reboot_reboot_view.xml',
        'wizard/do_reboot_view.xml'
    ],
    'demo': [
        'data/demo.xml'
    ]
}
