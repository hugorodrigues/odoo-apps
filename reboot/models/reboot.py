"""Reboots Odoo"""
# -*- coding: utf-8 -*-
import sys
import os
import logging

import openerp
from openerp import models, api, fields, exceptions, _, SUPERUSER_ID

_logger = logging.getLogger(__name__)

class Reboot(models.AbstractModel):
    """Reboots Odoo"""
    _name = 'reboot.reboot'

    def _do_reboot(self):
        os.execv(sys.executable, ['python3'] + sys.argv)

    @api.model
    def reboot(self, motive):
        """
        Reboots Odoo saving the motive

        :param string motive: Reboot motive
        """
        if self.env.user.has_group('reboot.reboot_admin'):
            log = self.env['reboot.log'].create({'motive': motive,
                                                 'state': 'pending'})
            self.env.cr.commit()  # Ensure that log is created
            _logger.info('%s rebooted Odoo with motive %s',
                         self.env.user.name, motive)
            try:
                self._do_reboot()
            except Exception as Ex:  # noqa: E772
                if isinstance(Ex, exceptions.except_orm):
                    message = Ex.name
                elif hasattr(Ex, 'message') and Ex.message:
                    message = Ex.message
                else:
                    message = str(Ex)
                log.write({'state': 'failed', 'fail_message': message})
                _logger.warning('Reboot failed: %s', message)
                self.env.cr.commit()  # Ensure that log is updated
                raise exceptions.ValidationError(message)
        else:
            raise exceptions.AccessError(_("You aren't allowed to reboot"))


class RebootLog(models.Model):
    """Logs when Odoo was rebooted"""
    _name = 'reboot.log'
    _order = 'create_date desc'

    def _register_hook(self):
        """Update all pending reboots to complete"""

        pending = self.env[self._name].search([('state', '=', 'pending')])
        if pending:
            pending.write({'state': 'complete'})
            _logger.info('Updated %s pending reboots', len(pending))

    motive = fields.Char(required=True)

    state = fields.Selection(
        selection=[
            ('pending', 'Pending'),
            ('complete', 'Complete'),
            ('failed', 'Failed')],
        required=True
    )

    fail_message = fields.Text(readonly=True)

    _sql_constraints = [
        ('motive_min_length', 'CHECK (LENGTH(motive) >= 10)',
         _('Motive must have at least 10 characters'))]

    @api.multi
    def write(self, vals):
        """Disable updates"""
        def _has_failed():
            return len(vals.keys()) == 2 and \
                    all(k in vals for k in ('state', 'fail_message')) and \
                    vals['state'] == 'failed'

        def _has_complete():
            return len(vals.keys()) == 1 and \
                    'state' in vals and \
                    vals['state'] == 'complete'

        #  Only allows write if a reboot failed
        is_pending = [x.state == "pending" for x in self]
        if all(is_pending) and (_has_failed() or _has_complete()):
            return super(RebootLog, self).write(vals)
        else:
            raise exceptions.ValidationError(_("You can't edit a reboot log"))

    @api.multi
    def copy(self, default=None):
        """Disable copy"""
        raise exceptions.ValidationError(_("You can't duplicate a log"))

    @api.multi
    def unlink(self):
        """Disables delete"""
        raise exceptions.ValidationError(_("You can't delete a log"))

    @api.multi
    def name_get(self):
        """Set record name"""
        res = []
        for log in self:
            name = '{0} - {1}'.format(self.create_uid.name, self.motive)
            res.append((log.id, name))
        return res
